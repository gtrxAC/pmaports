# Automatically generated aport, do not edit!
# Generator: pmbootstrap aportgen gcc-aarch64
# Based on: main/gcc (from Alpine)

CTARGET_ARCH=aarch64
CTARGET="$(arch_to_hostspec ${CTARGET_ARCH})"
LANG_D=false
LANG_OBJC=false
LANG_JAVA=false
LANG_GO=false
LANG_FORTRAN=false
LANG_ADA=false
options="!strip"

# abuild doesn't try to tries to install "build-base-$CTARGET_ARCH"
# when this variable matches "no*"
BOOTSTRAP="nobuildbase"

# abuild will only cross compile when this variable is set, but it
# needs to find a valid package database in there for dependency
# resolving, so we set it to /.
CBUILDROOT="/"

_cross_configure="--disable-bootstrap --with-sysroot=/usr/$CTARGET"

pkgname=gcc-aarch64
_pkgbase=13.1.1 # must match gcc/BASE-VER
_pkgsnap=20230513
pkgver=${_pkgbase}_git$_pkgsnap
[ "$BOOTSTRAP" = "nolibc" ] && pkgname="gcc-pass2"
[ "$CBUILD" != "$CHOST" ] && _cross="-$CARCH" || _cross=""
[ "$CHOST" != "$CTARGET" ] && _target="-$CTARGET_ARCH" || _target=""

pkgname=gcc-aarch64
pkgrel=2
pkgdesc="Stage2 cross-compiler for aarch64"
url="https://gcc.gnu.org"
arch="x86_64"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
_gccrel=$pkgver-r$pkgrel
depends="binutils-aarch64 mpc1"
makedepends_build="gcc g++ bison flex texinfo gawk zip gmp-dev mpfr-dev mpc1-dev zlib-dev"
makedepends_host="linux-headers gmp-dev mpfr-dev mpc1-dev isl-dev zlib-dev musl-dev-aarch64 binutils-aarch64"
subpackages="g++-aarch64:gpp libstdc++-dev-aarch64:libcxx_dev"
[ "$CHOST" = "$CTARGET" ] && subpackages="gcc-gdb gcc-doc$_target"
replaces="libstdc++ binutils"

: "${LANG_CXX:=true}"
: "${LANG_D:=true}"
: "${LANG_OBJC:=true}"
: "${LANG_GO:=true}"
: "${LANG_FORTRAN:=true}"
: "${LANG_ADA:=true}"
: "${LANG_JIT:=true}"

_libgomp=true
_libgcc=false
_libatomic=true
_libitm=true

if [ "$CHOST" != "$CTARGET" ]; then
	if [ "$BOOTSTRAP" = nolibc ]; then
		LANG_CXX=false
		LANG_ADA=false
		_libgcc=false
		_builddir="$srcdir/build-cross-pass2"
	else
		_builddir="$srcdir/build-cross-final"
	fi
	LANG_OBJC=false
	LANG_GO=false
	LANG_FORTRAN=false
	LANG_D=false
	LANG_JIT=false
	_libgomp=false
	_libatomic=false
	_libitm=false

	# format-sec: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=100431
	CPPFLAGS="${CPPFLAGS/-Werror=format-security/}"
	# reset target flags (should be set in crosscreate abuild)
	# fixup flags. seems gcc treats CPPFLAGS as global without
	# _FOR_xxx variants. wrap it in CFLAGS and CXXFLAGS.
	export CFLAGS="$CPPFLAGS -g0 ${CFLAGS/-Werror=format-security/}"
	export CXXFLAGS="$CPPFLAGS -g0 ${CXXFLAGS/-Werror=format-security/}"
	unset CPPFLAGS
	export CFLAGS_FOR_TARGET=" "
	export CXXFLAGS_FOR_TARGET=" "
	export LDFLAGS_FOR_TARGET=" "

	STRIP_FOR_TARGET="$CTARGET-strip"
elif [ "$CBUILD" != "$CHOST" ]; then
	# format-sec: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=100431
	CPPFLAGS="${CPPFLAGS/-Werror=format-security/}"
	# fixup flags. seems gcc treats CPPFLAGS as global without
	# _FOR_xxx variants. wrap it in CFLAGS and CXXFLAGS.
	export CFLAGS="$CPPFLAGS -g0 ${CFLAGS/-Werror=format-security/}"
	export CXXFLAGS="$CPPFLAGS -g0 ${CXXFLAGS/-Werror=format-security/}"
	unset CPPFLAGS

	# reset flags and cc for build
	export CC_FOR_BUILD="gcc"
	export CXX_FOR_BUILD="g++"
	export CFLAGS_FOR_BUILD=" "
	export CXXFLAGS_FOR_BUILD=" "
	export LDFLAGS_FOR_BUILD=" "
	export CFLAGS_FOR_TARGET=" "
	export CXXFLAGS_FOR_TARGET=" "
	export LDFLAGS_FOR_TARGET=" "

	# Languages that do not need bootstrapping
	LANG_OBJC=false
	LANG_GO=false
	LANG_FORTRAN=false
	LANG_D=false
	LANG_JIT=false

	STRIP_FOR_TARGET=${CROSS_COMPILE}strip
	_builddir="$srcdir/build-cross-native"
else
	STRIP_FOR_TARGET=${CROSS_COMPILE}strip
	_builddir="$srcdir/build"

	# format-sec: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=100431
	CPPFLAGS="${CPPFLAGS/-Werror=format-security/}"
	# pass -g0 by default to bypass -g, since we don't do debug
	# if -dbg added, the -g is appended and overrides this
	export CFLAGS="$CPPFLAGS -g0 ${CFLAGS/-Werror=format-security/} -O2"
	export CXXFLAGS="$CPPFLAGS -g0 ${CXXFLAGS/-Werror=format-security/} -O2"
	unset CPPFLAGS
	# https://gcc.gnu.org/install/build.html
	export CFLAGS_FOR_TARGET="$CFLAGS"
	export CXXFLAGS_FOR_TARGET="$CXXFLAGS"
	export LDFLAGS_FOR_TARGET="$LDFLAGS"
	export BOOT_CFLAGS="$CFLAGS"
	export BOOT_CXXFLAGS="$CXXFLAGS"
	export BOOT_LDFLAGS="$LDFLAGS"
fi

case "$CARCH" in
# GDC hasn't been ported to PowerPC
# See libphobos/configure.tgt in GCC sources for supported targets
# riscv fails with: error: static assert  "unimplemented"
ppc64le|riscv64)	LANG_D=false ;;
# GDC does currently not work on 32-bit musl architectures.
# This is a known upstream issue.
# See: https://github.com/dlang/druntime/pull/3383
armhf|armv7|x86)	LANG_D=false ;;
esac

# libitm has TEXTRELs in ARM build, so disable for now
case "$CTARGET_ARCH" in
arm*)		_libitm=false ;;
mips*)		_libitm=false ;;
riscv64)	_libitm=false ;;
esac

# Internal libffi fails to build on MIPS at the moment, need to
# investigate further.  We disable LANG_GO on mips64 as it requires
# the internal libffi.
case "$CTARGET_ARCH" in
mips*)		LANG_GO=false ;;
esac

# Fortran uses libquadmath if toolchain has __float128
# currently on x86, x86_64 and ia64
_libquadmath=$LANG_FORTRAN
case "$CTARGET_ARCH" in
x86 | x86_64 | ppc64le)	_libquadmath=$LANG_FORTRAN ;;
*)		_libquadmath=false ;;
esac

# libatomic is a dependency for openvswitch
$_libatomic && subpackages="$subpackages libatomic::$CTARGET_ARCH"
$_libgcc && subpackages="$subpackages libgcc::$CTARGET_ARCH"
$_libquadmath && subpackages="$subpackages libquadmath::$CTARGET_ARCH"
if $_libgomp; then
	depends="$depends libgomp=$_gccrel"
	subpackages="$subpackages libgomp::$CTARGET_ARCH"
fi

case "$CARCH" in
riscv64)
LANG_ADA=false;;
esac

_languages=c
if $LANG_CXX; then
	_languages="$_languages,c++"
fi
if $LANG_D; then
	subpackages="$subpackages libgphobos::$CTARGET_ARCH gcc-gdc$_target:gdc"
	_languages="$_languages,d"
	makedepends_build="$makedepends_build libucontext-dev gcc-gdc-bootstrap"
fi
if $LANG_OBJC; then
	subpackages="$subpackages libobjc::$CTARGET_ARCH gcc-objc$_target:objc"
	_languages="$_languages,objc"
fi
if $LANG_GO; then
	subpackages="$subpackages libgo::$CTARGET_ARCH gcc-go$_target:go"
	_languages="$_languages,go"
fi
if $LANG_FORTRAN; then
	subpackages="$subpackages libgfortran::$CTARGET_ARCH gfortran$_target:gfortran"
	_languages="$_languages,fortran"
fi
if $LANG_ADA; then
	subpackages="$subpackages gcc-gnat$_target:gnat"
	_languages="$_languages,ada"
	if [ "$CBUILD" = "$CTARGET" ]; then
		makedepends_build="$makedepends_build gcc-gnat-bootstrap"
		subpackages="$subpackages libgnat-static:libgnatstatic:$CTARGET_ARCH libgnat::$CTARGET_ARCH"
	else
		subpackages="$subpackages libgnat::$CTARGET_ARCH"
		makedepends_build="$makedepends_build gcc-gnat gcc-gnat$_cross"
	fi
fi
if $LANG_JIT; then
	subpackages="$subpackages libgccjit:jit libgccjit-dev:jitdev"
fi
makedepends="$makedepends_build $makedepends_host"

# when using upstream releases, use this URI template
# https://gcc.gnu.org/pub/gcc/releases/gcc-${_pkgbase:-$pkgver}/gcc-${_pkgbase:-$pkgver}.tar.xz
#
# right now, we are using a git snapshot. snapshots are taken from gcc.gnu.org/pub/gcc/snapshots.
# However, since they are periodically deleted from the GCC mirrors the utilized snapshots are
# mirrored on dev.alpinelinux.org. Please ensure that the snapshot Git commit (as stated in the
# README) matches the base commit on the version-specific branch in the Git repository below.
#
# PLEASE submit all patches to gcc to https://gitlab.alpinelinux.org/kaniini/alpine-gcc-patches,
# so that they can be properly tracked and easily rebased if needed.
source="https://dev.alpinelinux.org/archive/gcc/${_pkgbase%%.*}-$_pkgsnap/gcc-${_pkgbase%%.*}-$_pkgsnap.tar.xz
	0001-posix_memalign.patch
	0002-gcc-poison-system-directories.patch
	0003-specs-turn-on-Wl-z-now-by-default.patch
	0004-Turn-on-D_FORTIFY_SOURCE-2-by-default-for-C-C-ObjC-O.patch
	0005-On-linux-targets-pass-as-needed-by-default-to-the-li.patch
	0006-Enable-Wformat-and-Wformat-security-by-default.patch
	0007-Enable-Wtrampolines-by-default.patch
	0008-Disable-ssp-on-nostdlib-nodefaultlibs-and-ffreestand.patch
	0009-Ensure-that-msgfmt-doesn-t-encounter-problems-during.patch
	0010-Don-t-declare-asprintf-if-defined-as-a-macro.patch
	0011-libiberty-copy-PIC-objects-during-build-process.patch
	0012-libgcc_s.patch
	0013-nopie.patch
	0014-ada-fix-shared-linking.patch
	0015-build-fix-CXXFLAGS_FOR_BUILD-passing.patch
	0016-add-fortify-headers-paths.patch
	0017-Alpine-musl-package-provides-libssp_nonshared.a.-We-.patch
	0018-DP-Use-push-state-pop-state-for-gold-as-well-when-li.patch
	0019-aarch64-disable-multilib-support.patch
	0020-s390x-disable-multilib-support.patch
	0021-ppc64-le-disable-multilib-support.patch
	0022-x86_64-disable-multilib-support.patch
	0023-riscv-disable-multilib-support.patch
	0024-always-build-libgcc_eh.a.patch
	0025-ada-libgnarl-compatibility-for-musl.patch
	0026-ada-musl-support-fixes.patch
	0027-configure-Add-enable-autolink-libatomic-use-in-LINK_.patch
	0028-configure-fix-detection-of-atomic-builtins-in-libato.patch
	0029-libstdc-do-not-throw-exceptions-for-non-C-locales-on.patch
	0030-gdc-unconditionally-link-libgphobos-against-libucont.patch
	0031-druntime-link-against-libucontext-on-all-platforms.patch
	0032-libgnat-time_t-is-always-64-bit-on-musl-libc.patch
	0033-fix-assert-in-__deregister_frame_info_bases.patch
	"

# we build out-of-tree
_gccdir="$srcdir"/gcc-${_pkgbase%%.*}-$_pkgsnap
_gcclibdir="/usr/lib/gcc/$CTARGET/${_pkgbase:-$pkgver}"
_gcclibexec="/usr/libexec/gcc/$CTARGET/${_pkgbase:-$pkgver}"

prepare() {
	cd "$_gccdir"

	_err=
	for i in $source; do
		case "$i" in
		*.patch)
			msg "Applying $i"
			patch -p1 -i "$srcdir"/$i || _err="$_err $i"
			;;
		esac
	done

	if [ -n "$_err" ]; then
		error "The following patches failed:"
		for i in $_err; do
			echo "  $i"
		done
		return 1
	fi

	echo ${_pkgbase:-$pkgver} > gcc/BASE-VER
}

build() {
	local _arch_configure=
	local _libc_configure=
	local _bootstrap_configure=
	local _symvers=
	local _jit_configure=

	cd "$_gccdir"

	case "$CTARGET" in
	aarch64-*-*-*)		_arch_configure="--with-arch=armv8-a --with-abi=lp64";;
	armv5-*-*-*eabi)	_arch_configure="--with-arch=armv5te --with-tune=arm926ej-s --with-float=soft --with-abi=aapcs-linux";;
	armv6-*-*-*eabihf)	_arch_configure="--with-arch=armv6kz --with-tune=arm1176jzf-s --with-fpu=vfpv2 --with-float=hard --with-abi=aapcs-linux";;
	armv7-*-*-*eabihf)	_arch_configure="--with-arch=armv7-a --with-tune=generic-armv7-a --with-fpu=vfpv3-d16 --with-float=hard --with-abi=aapcs-linux --with-mode=thumb";;
	mips-*-*-*)		_arch_configure="--with-arch=mips32 --with-mips-plt --with-float=soft --with-abi=32";;
	mips64-*-*-*)		_arch_configure="--with-arch=mips3 --with-tune=mips64 --with-mips-plt --with-float=soft --with-abi=64";;
	mips64el-*-*-*)		_arch_configure="--with-arch=mips3 --with-tune=mips64 --with-mips-plt --with-float=soft --with-abi=64";;
	mipsel-*-*-*)		_arch_configure="--with-arch=mips32 --with-mips-plt --with-float=soft --with-abi=32";;
	powerpc-*-*-*)		_arch_configure="--enable-secureplt --enable-decimal-float=no";;
	powerpc64*-*-*-*)	_arch_configure="--with-abi=elfv2 --enable-secureplt --enable-decimal-float=no --enable-targets=powerpcle-linux";;
	i486-*-*-*)		_arch_configure="--with-arch=i486 --with-tune=generic --enable-cld";;
	i586-*-*-*)		_arch_configure="--with-arch=i586 --with-tune=generic --enable-cld";;
	s390x-*-*-*)		_arch_configure="--with-arch=z196 --with-tune=zEC12 --with-zarch --with-long-double-128 --enable-decimal-float";;
	riscv64-*-*-*)		_arch_configure="--with-arch=rv64gc --with-abi=lp64d --enable-autolink-libatomic";;
	esac

	case "$CTARGET_ARCH" in
	mips*)	_hash_style_configure="--with-linker-hash-style=sysv" ;;
	*)	_hash_style_configure="--with-linker-hash-style=gnu" ;;
	esac

	case "$CTARGET_LIBC" in
	musl)
		# musl does not support libsanitizer
		# alpine musl provides libssp_nonshared.a, so we don't need libssp either
		_libc_configure="--disable-libssp --disable-libsanitizer"
		_symvers="--disable-symvers"
		export libat_cv_have_ifunc=no
		;;
	esac


	case "$BOOTSTRAP" in
	nolibc)	_bootstrap_configure="--with-newlib --disable-shared --enable-threads=no" ;;
	*)	_bootstrap_configure="--enable-shared --enable-threads --enable-tls" ;;
	esac

	$_libgomp	|| _bootstrap_configure="$_bootstrap_configure --disable-libgomp"
	$_libatomic	|| _bootstrap_configure="$_bootstrap_configure --disable-libatomic"
	$_libitm	|| _bootstrap_configure="$_bootstrap_configure --disable-libitm"
	$_libquadmath	|| _arch_configure="$_arch_configure --disable-libquadmath"

	msg "Building the following:"
	echo ""
	echo "  CBUILD=$CBUILD"
	echo "  CHOST=$CHOST"
	echo "  CTARGET=$CTARGET"
	echo "  CTARGET_ARCH=$CTARGET_ARCH"
	echo "  CTARGET_LIBC=$CTARGET_LIBC"
	echo "  languages=$_languages"
	echo "  arch_configure=$_arch_configure"
	echo "  libc_configure=$_libc_configure"
	echo "  cross_configure=$_cross_configure"
	echo "  bootstrap_configure=$_bootstrap_configure"
	echo "  hash_style_configure=$_hash_style_configure"
	echo ""

	local version="Alpine $pkgver"
	local gccconfiguration="
		--prefix=/usr
		--mandir=/usr/share/man
		--infodir=/usr/share/info
		--build=$CBUILD
		--host=$CHOST
		--target=$CTARGET
		--enable-checking=release
		--disable-cet
		--disable-fixed-point
		--disable-libstdcxx-pch
		--disable-multilib
		--disable-nls
		--disable-werror
		$_symvers
		--enable-__cxa_atexit
		--enable-default-pie
		--enable-default-ssp
		--enable-languages=$_languages
		--enable-link-serialization=2
		--enable-linker-build-id
		$_arch_configure
		$_libc_configure
		$_cross_configure
		$_bootstrap_configure
		--with-bugurl=https://gitlab.alpinelinux.org/alpine/aports/-/issues
		--with-system-zlib
		$_hash_style_configure
		"

	mkdir -p "$_builddir"
	cd "$_builddir"
	"$_gccdir"/configure $gccconfiguration \
		--with-pkgversion="$version"

	msg "building gcc"
	make

	# we build gccjit separate to not build all of gcc with --enable-host-shared
	# as doing so slows it down a few %, so for some quick if's here we gain
	# free performance
	if $LANG_JIT; then
		mkdir -p "$_builddir"/libgccjit-build
		cd "$_builddir"/libgccjit-build
		"$_gccdir"/configure $gccconfiguration \
			--disable-bootstrap \
			--enable-host-shared \
			--enable-languages=jit \
			--with-pkgversion="$version"

		msg "building libgccjit"
		make all-gcc
	fi
}

package() {
	cd "$_builddir"
	make DESTDIR="$pkgdir" install

	ln -s gcc "$pkgdir"/usr/bin/cc

	if $LANG_JIT; then
		make -C "$_builddir"/libgccjit-build/gcc DESTDIR="$pkgdir" jit.install-common
	fi

	# we dont support gcj -static
	# and saving 35MB is not bad.
	find "$pkgdir" \( -name libgtkpeer.a \
		-o -name libgjsmalsa.a \
		-o -name libgij.a \) \
		-delete

	# strip debug info from some static libs
	find "$pkgdir" \( -name libgfortran.a -o -name libobjc.a -o -name libgomp.a \
		-o -name libgphobos.a -o -name libgdruntime.a \
		-o -name libgcc.a -o -name libgcov.a -o -name libquadmath.a \
		-o -name libitm.a -o -name libgo.a -o -name libcaf\*.a \
		-o -name libatomic.a -o -name libasan.a -o -name libtsan.a \) \
		-a -type f \
		-exec $STRIP_FOR_TARGET -g {} +

	if $_libgomp; then
		mv "$pkgdir"/usr/lib/libgomp.spec "$pkgdir"/$_gcclibdir
	fi
	if $_libitm; then
		mv "$pkgdir"/usr/lib/libitm.spec "$pkgdir"/$_gcclibdir
	fi

	# remove ffi
	rm -f "$pkgdir"/usr/lib/libffi* "$pkgdir"/usr/share/man/man3/ffi*
	find "$pkgdir" -name 'ffi*.h' -delete

	local gdblib=${_target:+$CTARGET/}lib
	if [ -d "$pkgdir"/usr/$gdblib/ ]; then
		for i in $(find "$pkgdir"/usr/$gdblib/ -type f -maxdepth 1 -name "*-gdb.py"); do
			mkdir -p "$pkgdir"/usr/share/gdb/python/auto-load/usr/$gdblib
			mv "$i" "$pkgdir"/usr/share/gdb/python/auto-load/usr/$gdblib/
		done
	fi

	# move ada runtime libs
	if $LANG_ADA; then
		for i in $(find "$pkgdir"/$_gcclibdir/adalib/ -type f -maxdepth 1 -name "libgna*.so"); do
			mv "$i" "$pkgdir"/usr/lib/
			ln -s ../../../../${i##*/} $i
		done
		if [ "$CHOST" = "$CTARGET" ]; then
			for i in $(find "$pkgdir"/$_gcclibdir/adalib/ -type f -maxdepth 1 -name "libgna*.a"); do
				mv "$i" "$pkgdir"/usr/lib/
				ln -s ../../../../${i##*/} $i
			done
		fi
	fi

	if [ "$CHOST" != "$CTARGET" ]; then
		# cross-gcc: remove any files that would conflict with the
		# native gcc package
		rm -rf "$pkgdir"/usr/bin/cc "$pkgdir"/usr/include "${pkgdir:?}"/usr/share
		# libcc1 does not depend on target, don't ship it
		rm -rf "$pkgdir"/usr/lib/libcc1.so*

		# fixup gcc library symlinks to be linker scripts so
		# linker finds the libs from relocated sysroot
		for so in "$pkgdir"/usr/"$CTARGET"/lib/*.so; do
			if [ -h "$so" ]; then
				local _real=$(basename "$(readlink "$so")")
				rm -f "$so"
				echo "GROUP ($_real)" > "$so"
			fi
		done
	else
		# add c89/c99 wrapper scripts
		cat >"$pkgdir"/usr/bin/c89 <<'EOF'
#!/bin/sh
_flavor="-std=c89"
for opt; do
	case "$opt" in
	-ansi|-std=c89|-std=iso9899:1990) _flavor="";;
	-std=*) echo "$(basename $0) called with non ANSI/ISO C option $opt" >&2
		exit 1;;
	esac
done
exec gcc $_flavor ${1+"$@"}
EOF
		cat >"$pkgdir"/usr/bin/c99 <<'EOF'
#!/bin/sh
_flavor="-std=c99"
for opt; do
	case "$opt" in
	-std=c99|-std=iso9899:1999) _flavor="";;
	-std=*) echo "$(basename $0) called with non ISO C99 option $opt" >&2
		exit 1;;
	esac
done
exec gcc $_flavor ${1+"$@"}
EOF
		chmod 755 "$pkgdir"/usr/bin/c?9

		# install lto plugin so regular binutils may use it
		mkdir -p "$pkgdir"/usr/lib/bfd-plugins
		ln -s /$_gcclibexec/liblto_plugin.so "$pkgdir/usr/lib/bfd-plugins/"
	fi
}

libatomic() {
	pkgdesc="GCC Atomic library"
	depends=
	replaces="gcc"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libatomic.so.* "$subpkgdir"/usr/lib/
}

libcxx() {
	pkgdesc="GNU C++ standard runtime library"
	depends=

	if [ "$CHOST" = "$CTARGET" ]; then
		# verify that we are using clock_gettime rather than doing direct syscalls
		# so we dont break 32 bit arches due to time64.
		nm -D "$pkgdir"/usr/lib/libstdc++.so.* | grep clock_gettime
	fi

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libstdc++.so.* "$subpkgdir"/usr/lib/
}

libcxx_dev() {
	pkgdesc="GNU C++ standard runtime library (development files)"
	depends=
	replaces="g++"

	amove usr/${_target:+$CTARGET/}lib/libstdc++.a \
		usr/${_target:+$CTARGET/}lib/libstdc++exp.a \
		usr/${_target:+$CTARGET/}lib/libstdc++.so \
		usr/${_target:+$CTARGET/}lib/libstdc++fs.a \
		usr/${_target:+$CTARGET/}lib/libsupc++.a \
		usr/${_target:+$CTARGET/}include/c++
}

gpp() {
	pkgdesc="GNU C++ standard library and compiler"
	depends="libstdc++=$_gccrel libstdc++-dev$_target=$_gccrel gcc$_target=$_gccrel libc-dev"
	mkdir -p "$subpkgdir/$_gcclibexec" \
		"$subpkgdir"/usr/bin \
		"$subpkgdir"/usr/${_target:+$CTARGET/}include \
		"$subpkgdir"/usr/${_target:+$CTARGET/}lib \

	mv "$pkgdir/$_gcclibexec/cc1plus" "$subpkgdir/$_gcclibexec/"

	mv "$pkgdir"/usr/bin/*++ "$subpkgdir"/usr/bin/
}

jit() {
	pkgdesc="GCC JIT Library"
	depends=
	amove usr/lib/libgccjit.so*
}

jitdev() {
	pkgdesc="GCC JIT Library (development files)"
	depends="libgccjit"
	amove usr/include/libgccjit*.h
}

libobjc() {
	pkgdesc="GNU Objective-C runtime"
	replaces="objc"
	depends=
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libobjc.so.* "$subpkgdir"/usr/lib/
}

objc() {
	pkgdesc="GNU Objective-C"
	replaces="gcc"
	depends="libc-dev gcc=$_gccrel libobjc=$_gccrel"

	mkdir -p "$subpkgdir/$_gcclibexec" \
		"$subpkgdir"/$_gcclibdir/include \
		"$subpkgdir"/usr/lib
	mv "$pkgdir/$_gcclibexec/cc1obj" "$subpkgdir/$_gcclibexec/"
	mv "$pkgdir"/$_gcclibdir/include/objc "$subpkgdir"/$_gcclibdir/include/
	mv "$pkgdir"/usr/lib/libobjc.so "$pkgdir"/usr/lib/libobjc.a \
		"$subpkgdir"/usr/lib/
}

libgcc() {
	pkgdesc="GNU C compiler runtime libraries"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libgcc_s.so.* "$subpkgdir"/usr/lib/
}

libgomp() {
	pkgdesc="GCC shared-memory parallel programming API library"
	depends=
	replaces="gcc"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/${_target:+$CTARGET/}lib/libgomp.so.* "$subpkgdir"/usr/lib/
}

libgphobos() {
	pkgdesc="D programming language standard library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgdruntime.so.* "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgphobos.so.*  "$subpkgdir"/usr/lib/
}

gdc() {
	pkgdesc="GCC-based D language compiler"
	depends="gcc=$_gccrel libgphobos=$_gccrel musl-dev"
	depends="$depends libucontext-dev"
	provides="gcc-gdc-bootstrap=$_gccrel"

	mkdir -p "$subpkgdir/$_gcclibexec" \
		"$subpkgdir"/$_gcclibdir/include/d/ \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	# Copy: The installed '.d' files, the static lib, the binary itself
	# The shared libs are part of 'libgphobos' so one can run program
	# without installing the compiler
	mv "$pkgdir/$_gcclibexec/d21" "$subpkgdir/$_gcclibexec/"
	mv "$pkgdir"/$_gcclibdir/include/d/* "$subpkgdir"/$_gcclibdir/include/d/
	mv "$pkgdir"/usr/lib/libgdruntime.a "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgdruntime.so "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgphobos.a "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgphobos.so "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/libgphobos.spec "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/bin/$CTARGET-gdc "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/bin/gdc "$subpkgdir"/usr/bin/
}

libgo() {
	pkgdesc="Go runtime library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgo.so.* "$subpkgdir"/usr/lib/
}

go() {
	pkgdesc="GCC Go frontend (intended for bootstrapping community/go)"
	depends="gcc=$_gccrel libgo=$_gccrel !go"
	install="$pkgname-go.post-install"

	# See https://lists.alpinelinux.org/~alpine/devel/%3C33KG0XO61I4IL.2Z7RTAZ5J3SY6%408pit.net%3E
	provides="go-bootstrap"
	provider_priority=1 # lowest, see community/go

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/lib/go "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/bin/*gccgo "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/bin/*go "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/*gofmt "$subpkgdir"/usr/bin
	mv "$pkgdir"/$_gcclibexec/go1 "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/cgo "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/buildid "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/test2json "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibexec/vet "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/usr/lib/libgo.a \
		"$pkgdir"/usr/lib/libgo.so \
		"$pkgdir"/usr/lib/libgobegin.a \
		"$pkgdir"/usr/lib/libgolibbegin.a \
		"$subpkgdir"/usr/lib/
}

libgfortran() {
	pkgdesc="Fortran runtime library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgfortran.so.* "$subpkgdir"/usr/lib/
}

libquadmath() {
	replaces="gcc"
	pkgdesc="128-bit math library for GCC"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libquadmath.so.* "$subpkgdir"/usr/lib/
}

gfortran() {
	pkgdesc="GNU Fortran Compiler"
	depends="gcc=$_gccrel libgfortran=$_gccrel"
	$_libquadmath && depends="$depends libquadmath=$_gccrel"
	replaces="gcc"

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/$_gcclibdir \
		"$subpkgdir"/usr/lib \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/*gfortran "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/lib/libgfortran.a \
		"$pkgdir"/usr/lib/libgfortran.so \
		"$subpkgdir"/usr/lib/
	if $_libquadmath; then
		mv "$pkgdir"/usr/lib/libquadmath.a \
			"$pkgdir"/usr/lib/libquadmath.so \
			"$subpkgdir"/usr/lib/
	fi
	mv "$pkgdir"/$_gcclibdir/finclude "$subpkgdir"/$_gcclibdir/
	mv "$pkgdir"/$_gcclibexec/f951 "$subpkgdir"/$_gcclibexec
	mv "$pkgdir"/usr/lib/libgfortran.spec "$subpkgdir"/$_gcclibdir
}

libgnat() {
	pkgdesc="GNU Ada runtime shared libraries"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgna*.so "$subpkgdir"/usr/lib/
}

libgnatstatic() {
	pkgdesc="GNU Ada static libraries"
	depends=

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgna*.a "$subpkgdir"/usr/lib/
}

gnat() {
	pkgdesc="Ada support for GCC"
	depends="gcc=$_gccrel"
	provides="$pkgname-gnat-bootstrap=$_gccrel"
	[ "$CHOST" = "$CTARGET" ] && depends="$depends libgnat=$_gccrel"

	mkdir -p "$subpkgdir"/$_gcclibexec \
		"$subpkgdir"/$_gcclibdir \
		"$subpkgdir"/usr/bin
	mv "$pkgdir"/$_gcclibexec/*gnat* "$subpkgdir"/$_gcclibexec/
	mv "$pkgdir"/$_gcclibdir/*ada* "$subpkgdir"/$_gcclibdir/
	mv "$pkgdir"/usr/bin/*gnat* "$subpkgdir"/usr/bin/
}

gdb() {
	pkgdesc="$pkgdesc (gdb printers)"
	install_if="$pkgname=$pkgver-r$pkgrel gdb"

	amove \
		usr/share/gdb/python/ \
		usr/share/gcc-*/python/
}

sha512sums="
a82e38d273a668e4a8ad33282cb74661d125babcc6165eb0ada5da471154b2ed2894bf1fa462f0345f9d817e733b155c09a971885f3b7c04398ebcdd3b5551f5  gcc-13-20230513.tar.xz
275a71fcabc03209fe69894ee9c4bec57afabd170f08385fa148b4e481c0b2d2e5a3e2631e82551f21bc3f93c2b5299ddf39d09b88e1a5451d1cd4fb5ff3be06  0001-posix_memalign.patch
8c247b796c2cea016fe43b1c7393ed94567bd565e453fdf114d90800a86d1e5bb3761a26c9b48bccb7b3192ca482a72d02fa8b65e4703753ff8b567ddd8cb949  0002-gcc-poison-system-directories.patch
3c0773148638c41d2a45f43c8320bd9c7b1129650168f0f80bfe4cf9d00d11f352952f178c88285b5a5e47df76264304c05080b9df4948981c0d01bd7cce5cfa  0003-specs-turn-on-Wl-z-now-by-default.patch
cc81175e0e74abbb46dbc63d64c1a36fc62d4eae7ba3cc5ab85de331c31f3341fc445f3b5be7e1f53ee72e0e3a512232b821cef349a0a5991d46834d1325246e  0004-Turn-on-D_FORTIFY_SOURCE-2-by-default-for-C-C-ObjC-O.patch
c52396a1708b1b4a29e0b5a8d2843f4ea1eb2c34fd2fd9401da0c826a1192b6f87d8b5aea7429f4811c3e293c2fef1945ac05d7c82f3ce94c84f440bb772c76e  0005-On-linux-targets-pass-as-needed-by-default-to-the-li.patch
03f1ce1a6c90a438d4b953273ae31996fd54226b320f075cc5ebfe8f1c14958138922b8037774fc9a29d10c8b99303d8119a0075fbb45b608fc14f0b18ed3720  0006-Enable-Wformat-and-Wformat-security-by-default.patch
3c9f0cde600081e59e7542784839defa46e5bea7a19b9cfcfbf042f883a2a7a7547d0eeb6edea51cbb0682e20e25bde9e6597871d492cf5eaf9cfbd9475f7ce1  0007-Enable-Wtrampolines-by-default.patch
de8c97e379e9e85d57988ef3d2df2dd7f1f0a7e4e20c2bbc19f148bf3fbaccaf34e80fae8e8f60c6b9b4932135c14fd2f1a20624e44aa6d67ba92b29d36c6051  0008-Disable-ssp-on-nostdlib-nodefaultlibs-and-ffreestand.patch
acc01ac7a8da1eb02915b5b82dd539b1e8ac5045faeb6db02f54390f77a3d7a8f0cf65c2927a4150e7a52ee4a7df31091d325dd45641cc6639883defda7d0d42  0009-Ensure-that-msgfmt-doesn-t-encounter-problems-during.patch
101ef48ce43aa084ab9875c1917388e1ae2b3ef33d0d616429515a9ce1f6c9af28b13a6a59ef4a384b9834d0846b4d28f593857cffacda2ac04abe67ffe7ca8f  0010-Don-t-declare-asprintf-if-defined-as-a-macro.patch
6563f09d331048f402dc0c54e8be48ff701fe14316ad48c301a75368414f3cf1fb0e3ed706090fff70db6879b390c05bf2ed504206c1e407b9cc5f7295517183  0011-libiberty-copy-PIC-objects-during-build-process.patch
6ea5e476a67a16a9e94c5ed8040d1af4ebce4b417a22c97be978ff6fc91f7a627a35fc077c8ce7f5fdf0d20712001622abdccb117a7ffa789f9a90cf23cd1429  0012-libgcc_s.patch
53f96185583ca048010e759eb364ee646ebb1728379da4a7f6137a2009a3657f3d166cb9bfae78994070dd3750859a29857dfc6420bd9accebf629e259c096c2  0013-nopie.patch
d181e399bd2bdcbbbb69767ba08220d69ac97cd05faf2e1172266a1e194cf32ca3ff3a6bc29751223ecf9d2d739f6fca66dc825d4026ea6a272d39d98e038e2e  0014-ada-fix-shared-linking.patch
c08339b2f2c2840471f0ff7270d372ec5613113432c507519c3805df97215955532f53e5d9f4ca1ec345199fe7701c5e4fc1054b2bdac998c21add01feeaf94a  0015-build-fix-CXXFLAGS_FOR_BUILD-passing.patch
2ee36af2e4bcd1d7b9bba588d638f618359a3c60fca8939311f1e837551522fa1a6911175dfd87c56dcb4d42de8b04af65afc882ddcec215ee5f2a8c086ee399  0016-add-fortify-headers-paths.patch
af71729e9c21aaf13ab0ba9407b3be4b4e9c6b515a8e65686628ddd885ff6b69f8f31c714ce52b7c5a5c9b9d1f31070e6a4278b5d5d11868f152258002077355  0017-Alpine-musl-package-provides-libssp_nonshared.a.-We-.patch
dc37f213776957bd704caf481189063589e8ed7a223421a0489553421d45249e7425147755e344c4d172b35b329a2d191b420fd1cdd49de99546b874e5fbcd3d  0018-DP-Use-push-state-pop-state-for-gold-as-well-when-li.patch
4c9500ff6d479f84ce1ee9c043db4434b51b0f847914f78482e2fba915d0018bb19fb92882b3df8475cb70171183089115ce95c0e87f69c05e56d1e3b94262be  0019-aarch64-disable-multilib-support.patch
832eb922bcd8b4bd510929de520ea036734316c55c222a3323b3df26923ce599a8580586e7922860db7ecd2a0c7d9d15213daa1ae7f7026fe8ec8eac5ca29f32  0020-s390x-disable-multilib-support.patch
fe99502b6c12b923774486dfba7480959708722ca56690e8b601c7f774f074a21f8f3ff1a0b39ca0b88ebaa2b5773652f6c9fdf92d7f78a3e674038dcb98c5a6  0021-ppc64-le-disable-multilib-support.patch
50160fdacd1eb2696a87c63955308b07c21bf9ce5b085e74fdcc3db633cbe705e84e9d37bc124dfdbfd89980026533c2ca5a537fdfa3edfdcb46d50625df53ea  0022-x86_64-disable-multilib-support.patch
498fda16a87f11f802c4910a4e1cd8c02b6e302cccfc60588749a170e4729ac5054a1ae253772baf82854767659eedc07c5e6ae0e154b3ff24dc57a017de9f20  0023-riscv-disable-multilib-support.patch
c846962e05062e036dd6b5d107cadb9aa33019ca555936394605911de8a3b5a145f03cc59b393b2a42f7b8ded5712c80583d27c8ac4eec145dcd688a5dfc8b81  0024-always-build-libgcc_eh.a.patch
e476823a735ed41a1220b06e74c56312cd98077c4c4a06bc8af4875a25c197d3d38b0666765c2ed5b5e5d1c5093d456caf19cab5eb57cfb841797873d4fefe28  0025-ada-libgnarl-compatibility-for-musl.patch
1a120d66233b5becd89f1042252d43327aab975ccdfd06480f11f79e306bd9ac4869d7483bd2bf9a8125426bf01582f3f2524eec5d7ab4d762e7d7829070be01  0026-ada-musl-support-fixes.patch
1290e17df57e549e1de7431473389210102841e42bbafceeef5e69ec258ed4eb5a4fb2aa4ba1ccb66496833335412495000fd8be6c74b4edd633af352a54e10b  0027-configure-Add-enable-autolink-libatomic-use-in-LINK_.patch
39c6604aaa6e931f507b804c1d3443d59edfd66d4ba8774b02fd6d30937c85a645575ab7921c3c8ee5d51d65a76290b47319c5f0276fd4fb5319f1e94e875160  0028-configure-fix-detection-of-atomic-builtins-in-libato.patch
b3be6955a078ace099dceeb48535ab41ff47c79963822c93741f50823e0239e4aff26566e4abc9d2c3964f65bc7d61bf081398c5e6c4ba009db8d5d60dc8de10  0029-libstdc-do-not-throw-exceptions-for-non-C-locales-on.patch
028e67e52aa6ae31dadd9da4d235e780832e9a114d2ffb72b452dac81f80851bcbb652429c760daf70e07edfb80589953bf780b670313b367cc72877cc3d16e5  0030-gdc-unconditionally-link-libgphobos-against-libucont.patch
3d845c1684fb4286f9e8a57df860db9a9eb18296d6e54e09b53d33bce67b2529995b6611584eef7e37fd506c20ec1f9f839f107678614b1d1cde675a084495cc  0031-druntime-link-against-libucontext-on-all-platforms.patch
4929eeeb0ea962ca3d8d5bf00334bb520c4419a31940d79244bd8a75cf9a5ac53d60aed4728404c463bb4a82070f0224887281207f4f8f2e532f312c5071098b  0032-libgnat-time_t-is-always-64-bit-on-musl-libc.patch
97890297608a6f2926b9628321f3e27b5a86ca9f33933f66c9d2d4c441f4ed219867fbc18c2f51ad4e427ba799bd0ad7ff6927d0869ef9e1d594b72ad793657b  0033-fix-assert-in-__deregister_frame_info_bases.patch
"
